<?php
    include("../connection.php");
    $db = new dbObjects();
    $connection = $db->getConnection();
    $request_variable = $_SERVER['REQUEST_METHOD'];
    // echo $request_variable;

    switch($request_variable) {
        case 'GET':
            // Code for GET method - retrieving user or users from the DB
            if(empty($_GET['id']) || !isset($_GET['id'])) {
                get_employees();
            }else{
                $id = $_GET['id'];
                get_employee($id);
            }
            break;
        case 'POST':
            // Code for updating user in the db
            insert_employee();
        break;
        case 'PUT':
            // Code for updating user in the db
            $id = $_GET['id'];
            update_employee($id);
            break;
        case 'DELETE':
            // Code for deleting user in the db
            $id = $_GET['id'];
            delete_employee($id);
            break;
        default:
            echo "Something strange happened!";
            break;
    }

    function get_employees() {
        global $connection;
        $query = "SELECT * FROM employee";
        $response = array();
        $result = mysqli_query($connection, $query);
        while ($user = mysqli_fetch_assoc($result)) {
            $response[] = $user;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }
    function get_employee($id) {
        global $connection;
        $query = "SELECT * FROM employee WHERE id = '" . $id . "'";
        $response = array();
        $result = mysqli_query($connection, $query);
        if (mysqli_num_rows($result) > 0) {
            while ($user = mysqli_fetch_assoc($result)) {
                $response[] = $user;
            }
            header('Content-Type: application/json');
            echo json_encode($response);
        }else{
            echo 'No Data was found!';
        }
    }
    function insert_employee(){
        global $connection;
        $data = json_decode(file_get_contents('php://input'), true);
        $name = $data['employee_name'];
        $salary = $data['employee_salary'];
        $age = $data['employee_age'];
        $query = "INSERT INTO employee SET employee_name ='$name', employee_salary = '$salary', employee_age= '$age'";

        if (mysqli_query($connection, $query)){
            $response = array('status' => 1, 'message' => 'Employee is added successfully');
        }else{
            $response = array('status' => 1, 'message' => 'Employee is not added successfully');
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }
    function update_employee($id) {
        global $connection;
        $data = json_decode(file_get_contents('php://input'), true);
        $name = $data['employee_name'];
        $salary = $data['employee_salary'];
        $age = $data['employee_age'];
        $query = "UPDATE employee SET employee_name = '$name', employee_salary = $salary, employee_age = $age WHERE id = $id";

        if (mysqli_query($connection, $query)){
            $response = array('status' => 1, 'message' => 'Employee is updated successfully');
        }else{
            $response = array('status' => 1, 'message' => 'Employee is not updated successfully');
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }
    function delete_employee($id) {
        global $connection;
        $query = "DELETE FROM employee WHERE id = $id";

        if (mysqli_query($connection, $query)){
            $response = array('status' => 1, 'message' => 'Employee is deleted successfully');
        }else{
            $response = array('status' => 1, 'message' => 'Employee is not deleted successfully');
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }
?>