<?php
	class dbObjects
    {
    var $serverName = "localhost";
    var $userName = "root";
    var $password = "";
    var $dbName = "verteiltesysteme";
    var $conn;

        function getConnection()
        {
            $connection = mysqli_connect(
                $this->serverName,
                $this->userName,
                $this->password,
                $this->dbName
            ) or die ("Connection failed: " . mysqli_connect_error());

            //check connection
            if (mysqli_connect_errno()) {
                echo "Problems with connection: " . mysqli_connect_error();
                exit;
            } else {
                $this->conn = $connection;
            }
            return $this->conn;
        }
    }
    ?>