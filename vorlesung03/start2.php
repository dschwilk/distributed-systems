<html>
<head>
    <?php
        $url = "data2.json";
        $data = file_get_contents($url);

        //Mit True-Attribut wird json zu Array konvertiert json_decode($data, true)
        $persona = json_decode($data);
  ?>
  <style>
        tr:nth-child(even) { background-color: lightblue; }
        body { background-color: grey; }
        table { background-color: white; }
  </style>
</head>
<body>
    <table border="1">
        <tr>
            <th> Name </th>
            <th> Job </th>
        </tr>
    <?php
        foreach ($persona as $person) {
                echo '<tr>
                <td>' . $person->name . '</td>';
                foreach ($person->job as $jobBeschreibung) {        
                    echo '<td>' . $jobBeschreibung->salary . '</td>'; 
                }
            echo '</tr>'; 
        }
    ?>
</body>
</html>