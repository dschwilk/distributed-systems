<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <h4>Get data as JSON from a PHP file on the server</h4>
        <p id="carinfo"></p>
        <script>
            var xmlhttp = new XMLHttpRequest();
            
            //Create listener for XMLHttpRequest instance
            xmlhttp.onreadystatechange = function() {
                //status = http status (i.e. 200 or 404)
                //readState = 0: request is not initialized, 1: Server connection is established, 2: request is received, 3: processing request, 4: request finished and the response is ready
                if (this.readyState == 4 && this.status == 200) {
                    object = JSON.parse(this.responseText);
                    document.getElementById('carinfo').innerHTML = object.brand;
                }
            };
            //Open communication
            //true - asynchronuous communication
            //false - synchronous
            xmlhttp.open("GET", "../server.php", true);
            //Send actual request
            xmlhttp.send();
        </script>
    </body>
</html>