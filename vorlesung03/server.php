<?php
    class Car {
        public $brand;
        public $color;
        public $tires;
    }
    
    $blueCar = new Car();
    $blueCar->brand = "VW";
    $blueCar->color = "blue";
    $blueCar->tires = "tires";
    
    $myJSON = json_encode($blueCar);
    echo $myJSON;

?>