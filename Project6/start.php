<?php
  $url = "data2.json";
  $data = file_get_contents($url);
  $contents = json_decode($data);


echo '<!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title></title>
   </head>
   <body>
	<table border="1">
     <thead>
       <th>Questions</th>
       <th>Answers</th>
       <th>Result</th>
     </thead>
     <tbody>';

       foreach ($contents as $content) {
         echo '<tr>
         <td>' .
         $content->question .
         '</td>
		 <td>';
			 foreach ($content->options as $option) {
      		echo $option . '<br>';
			 }
         echo '</td>
         <td>' .
         $content->answer
         . '</td>
         </tr>';
       }

    echo '</tbody>
	</table>
   </body>
 </html>';
 ?>
